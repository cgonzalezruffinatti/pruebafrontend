interface ITableView {
    headers: string[];
    dataSet: {
        data: any,
        items: ITableItem[]
    }[];
}

interface ITableItem {
    text?: number | string;
    badge?: IBadge;
}

interface IBadge {
    text?: number | string;
    color?: string;
    tooltip?: string;
}