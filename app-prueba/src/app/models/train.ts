interface Train {
    shiftName: string;
    groupName: string;
    firstTrainDatetime: string;
    firstTrainRule: number;
    trainsTandemQty: number;
    trainsTandemRule: number;
    firstCicleDatetime: string;
    firstCicleTrains: number;
    gapMinutes: number;
    gapMinutesRule: number;
}