export interface IMinuteByMinute {
    shift: string;
    shiftStart: string;
    shiftEnd: string;
    machineId: number;
    machineName: string;
    completedAt: string;
    message: string;
}