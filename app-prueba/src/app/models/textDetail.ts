export class TextDetail {
  paragraph: string;
  number: number;
  hasCopyright: boolean;
}
