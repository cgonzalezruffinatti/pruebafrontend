export class LetterDetail {
  paragraph: string;
  letter: string;
  quantity: number;
  numbers?: string[];
}
