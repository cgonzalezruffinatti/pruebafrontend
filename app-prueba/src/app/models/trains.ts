interface Trains {
    turn: string;
    group: string;
    firstTrain: string;
    numTrains: string;
    cycleClosure: string;
    minuteDelta: string;
}