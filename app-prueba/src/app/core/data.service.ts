import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { DataResponse } from '../models/dataResponse';

@Injectable()
export class DataService {

  constructor(private httpClient: HttpClient) { }

  getDataVistaUno(): Observable<DataResponse> {
    return this.httpClient.get<DataResponse>('http://patovega.com/prueba_frontend/array.php', {
      headers: new HttpHeaders({
        Accept: 'application/json'
      })
    })
    .pipe(
      catchError(err => this.handleHttpError(err)),
      tap(res => console.log(res.data))
    );
  }

  getDataVistaDos(): Observable<DataResponse> {
    return this.httpClient.get<DataResponse>('http://patovega.com/prueba_frontend/dict.php', {
      headers: new HttpHeaders({
        Accept: 'application/json'
      })
    })
    .pipe(
      map(res => {
        return {
          ...res,
          data: JSON.parse(res.data)
        }
      }),
      catchError(err => this.handleHttpError(err)),
      tap(res => console.log(res))
    );
  }

  private handleHttpError(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error);
  }

}
