import { NgModule } from '@angular/core';
import { DataService } from './data.service';

@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    DataService
  ],
})
export class CoreModule { }
