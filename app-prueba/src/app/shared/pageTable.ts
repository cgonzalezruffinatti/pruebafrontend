export interface IPageTable {
    page: number;
    pageSize: number;
}