import { Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit, ViewChild, ViewChildren, QueryList, EventEmitter, Output } from '@angular/core';
import { NgbdSortableHeaderDirective } from '../ngbdSortableHeader';
import { IPageTable } from '../pageTable';
import { SortEvent } from '../sort-table';


@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnChanges, AfterViewInit {

    @Input()
    public tableView: ITableView;

    @Input()
    public onPageSize: number;

    @Input()
    public onCollectionSize: number;

    @Output() 
    pageTable = new EventEmitter<IPageTable>();

    @Output() 
    sortTable = new EventEmitter<SortEvent>();

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    private collectionSize;

    // current page
    private _page: number;
    get page(): number {
        return this._page;
    }
    set page(value: number) {
        console.log("set")
        this._page = value;
        this.pageTable.emit({ page: this.page, pageSize: this.pageSize })
    }

    // number of records to display
    private _pageSize: number;
    get pageSize(): number {
        return this._pageSize;
    }
    set pageSize(value: number) {
        console.log("set page size")
        this._pageSize = value;
        this.page = 1; // back to the first page
    }
 
    constructor() { 
        
    }

    ngOnInit() {
        this.page = 1; 
        this.pageSize = this.onPageSize;
        this.collectionSize = this.onCollectionSize;
        
    }

    ngAfterViewInit() {
    
    }

    ngOnChanges(changes: SimpleChanges) {
        
        // if (changes.tableView) {
        //     const tv: ITableView = changes.tableView.currentValue;
        // }
    }

    onSort(values: SortEvent) {
        console.log('headers:', this.headers);
        
        // resetting other headers
        this.headers.forEach(header => {
            if (header.sortable !== values.column) {
                header.direction = '';
            }
        });
        this.page = 1;
        this.sortTable.emit({ column: values.column, direction: values.direction, page: this.page, pageSize: this.pageSize  });
    }

}