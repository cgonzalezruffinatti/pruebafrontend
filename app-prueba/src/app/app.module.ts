import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { VistaUnoComponent } from './vista-uno/vista-uno.component';
import { AppRoutingModule } from './app-routing.module';
import { VistaDosComponent } from './vista-dos/vista-dos.component';
import { CoreModule } from './core/core.module';
import { TrainsComponent } from './trains/trains.component';
import { MaterialModule } from './shared/material/material.module';
import { MinuteByMinuteComponent } from './trains/minutebyminute.component';
import { NgbTableComponent } from './trains/ngbTable.component';
import { TableComponent } from './shared/table/table.component';
import { NgbdSortableHeaderDirective } from './shared/ngbdSortableHeader';
import { PersonsByZoneComponent } from './maps/persons-by-zone.component';

@NgModule({
  declarations: [
    AppComponent,
    VistaUnoComponent,
    VistaDosComponent,
    TrainsComponent,
    MinuteByMinuteComponent,
    //NgbdSortableHeader,
    NgbdSortableHeaderDirective,
    NgbTableComponent,
    TableComponent,
    PersonsByZoneComponent
  ],
  imports: [
    BrowserModule,
    //BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    NgbModule,
    CoreModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [//NgbdSortableHeader,
            NgbdSortableHeaderDirective]
})
export class AppModule { }
