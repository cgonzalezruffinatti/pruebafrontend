import { Component, OnInit } from '@angular/core';
import { DataService } from '../core/data.service';
import { DataResponse } from '../models/dataResponse';
import * as _ from 'lodash';
import { NumberDetail } from '../models/numberDetail';

@Component({
  selector: 'app-vista-uno',
  templateUrl: './vista-uno.component.html',
  styleUrls: ['./vista-uno.component.css']
})
export class VistaUnoComponent implements OnInit {

  processedNumbers: NumberDetail[];
  orderedNumbers: string;
  loading = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {

  }

  getData() {
    this.loading = true;
    this.dataService.getDataVistaUno()
    .subscribe((data: DataResponse) => {
      if (data) {
        this.processedNumbers = this.processNumbers(data.data);
        this.loading = false;
      }
    });
  }

  processNumbers(data): NumberDetail[] {
    const numbers = data || [];
    const filteredNumbers = Array.from(new Set(numbers)); // array sin números repetidos
    const countNumbers = _.countBy(numbers, (n) => Math.floor(n)); // retorna la cantidad de veces que esta repetido cada número
    const list: NumberDetail[] = [];

    filteredNumbers.forEach((num: number) => {
      const numObj = new NumberDetail();
      numObj.number = num;
      numObj.quantity = countNumbers[num];
      numObj.firstPosition = _.indexOf(numbers, num); // indice primer match encontrado
      numObj.lastPosition = _.lastIndexOf(numbers, num); // indice último match encontrado
      list.push(numObj);
    });

    this.orderedNumbers = _.sortBy(filteredNumbers).join(','); // se ordena arreglo de numeros de menor a mayor

    return list;
  }

  getColor(num: number): string {
    if (num >= 2) {
      return 'green';
    } else if (num === 1) {
      return 'yellow';
    } else if (num < 1) {
      return 'gray';
    } else {
      return '';
    }
  }

}
