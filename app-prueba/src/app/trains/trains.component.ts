import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

const TRAINS: Trains[] = [
  {
    turn: 'A',
    group: 'G3',
    firstTrain: '06:43:24',
    numTrains: '06',
    cycleClosure: '07:34:15',
    minuteDelta: '03'
  },
  {
    turn: 'B',
    group: 'G1',
    firstTrain: null,
    numTrains: '06',
    cycleClosure: null,
    minuteDelta: null
  },
  {
    turn: 'B',
    group: 'G1',
    firstTrain: null,
    numTrains: '06',
    cycleClosure: null,
    minuteDelta: null
  } 
]

const data: Train[] = [
  {
    "shiftName": "a",
    "groupName": "g3",
    "firstTrainDatetime": "2020-02-25T06:46:37.02-03:00",
    "firstTrainRule": 0,
    "trainsTandemQty": 6,
    "trainsTandemRule": 1,
    "firstCicleDatetime": "2020-02-25T07:34:15.02-03:00",
    "firstCicleTrains": 0,
    "gapMinutes": 3,
    "gapMinutesRule": 1
  },
  {
    "shiftName": "b",
    "groupName": "g2",
    "firstTrainDatetime": "0001-01-01T00:00:00+00:00",
    "firstTrainRule": 0,
    "trainsTandemQty": 0,
    "trainsTandemRule": 0,
    "firstCicleDatetime": "0001-01-01T00:00:00+00:00",
    "firstCicleTrains": 0,
    "gapMinutes": 0,
    "gapMinutesRule": 0
  },
  {
    "shiftName": "c",
    "groupName": "g1",
    "firstTrainDatetime": "2020-02-24T23:46:37.02-03:00",
    "firstTrainRule": 0,
    "trainsTandemQty": 5,
    "trainsTandemRule": 1,
    "firstCicleDatetime": "2020-02-25T00:34:15.02-03:00",
    "firstCicleTrains": 5,
    "gapMinutes": 8,
    "gapMinutesRule": 0
  }
]


@Component({
  selector: 'app-trains',
  templateUrl: './trains.component.html',
  styleUrls: ['./trains.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TrainsComponent implements OnInit {

  trains = TRAINS;

  displayedColumns: string[] = ['shiftName', 'groupName', 'firstTrainDatetime', 'trainsTandemQty', 'firstCicleDatetime', 'gapMinutes'];
  dataSource: MatTableDataSource<Train>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  constructor() {
    this.dataSource = new MatTableDataSource(data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  getTime(value:string) {
    console.log(value);
    var date = moment(value).format('hh:mm:ss');
    console.log(date);
    return date;
  }

}
