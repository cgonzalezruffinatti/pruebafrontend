import { Component, OnInit, Directive, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { IMinuteByMinute } from '../models/minute-by-minute';
import * as moment from 'moment';
import { NgbdSortableHeaderDirective } from '../shared/ngbdSortableHeader';
import { compare, SortEvent } from '../shared/sort-table';


const json = [
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T10:02:30.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T10:02:30.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 58,
        "machineName": "Loco 131",
        "completedAt": "2019-08-16T10:07:47.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T10:17:49.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 62,
        "machineName": "Loco 662",
        "completedAt": "2019-08-16T10:19:58.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T10:54:51.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T11:15:30.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T11:15:30.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T11:41:09.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T11:48:24.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T12:10:31.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T12:10:31.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 62,
        "machineName": "Loco 662",
        "completedAt": "2019-08-16T12:13:38.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 62,
        "machineName": "Loco 662",
        "completedAt": "2019-08-16T12:13:38.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T12:48:16.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T12:57:41.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T13:09:13.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T13:13:27.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T13:13:27.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T13:25:13.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T13:25:13.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T13:39:52.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T13:39:52.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T13:49:58.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T13:49:58.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T14:06:09.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 57,
        "machineName": "Loco 663",
        "completedAt": "2019-08-16T14:08:22.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 57,
        "machineName": "Loco 663",
        "completedAt": "2019-08-16T15:28:23.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 57,
        "machineName": "Loco 663",
        "completedAt": "2019-08-16T15:28:23.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T15:34:28.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T15:34:28.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T16:24:08.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 58,
        "machineName": "Loco 131",
        "completedAt": "2019-08-16T16:30:46.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 49,
        "machineName": "Loco 135",
        "completedAt": "2019-08-16T16:33:23.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T16:36:07.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T16:39:47.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 52,
        "machineName": "Loco 136",
        "completedAt": "2019-08-16T16:45:14.02-04:00",
        "message": "El tren ha entrado de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T17:27:29.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 48,
        "machineName": "Loco 134",
        "completedAt": "2019-08-16T17:27:29.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 58,
        "machineName": "Loco 131",
        "completedAt": "2019-08-16T17:33:36.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 58,
        "machineName": "Loco 131",
        "completedAt": "2019-08-16T17:33:36.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T17:44:05.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 55,
        "machineName": "Loco 140",
        "completedAt": "2019-08-16T17:44:05.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 49,
        "machineName": "Loco 135",
        "completedAt": "2019-08-16T17:48:32.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 49,
        "machineName": "Loco 135",
        "completedAt": "2019-08-16T17:48:32.02-04:00",
        "message": "El tren ha salido de la mina"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T17:56:53.02-04:00",
        "message": "El tren ha completado una vuelta"
    },
    {
        "shift": "A",
        "shiftStart": "2019-08-16T10:00:00-04:00",
        "shiftEnd": "2019-08-16T17:59:59.999-04:00",
        "machineId": 54,
        "machineName": "Loco 133",
        "completedAt": "2019-08-16T17:56:53.02-04:00",
        "message": "El tren ha salido de la mina"
    }
];

// @Directive({
//     selector: 'th[sortable]',
//     host: {
//         '[class.asc]': 'direction === "asc"',
//         '[class.desc]': 'direction === "desc"',
//         '(click)': 'rotate()'
//     }
// })
// export class NgbdSortableHeader {

//     @Input() sortable: string;
//     @Input() direction: SortDirection = '';
//     @Output() sort = new EventEmitter<SortEvent>();

//     rotate() {
//         this.direction = rotate[this.direction];
//         this.sort.emit({ column: this.sortable, direction: this.direction });
//     }
// }

// export interface SortEvent {
//     column: string;
//     direction: SortDirection;
// }

// export type SortDirection = 'asc' | 'desc' | '';
// const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
// export const compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

@Component({
    templateUrl: './minutebyminute.component.html',
    styleUrls: ['./minutebyminute.component.css']
})
export class MinuteByMinuteComponent implements OnInit {

    private tableData: ITableView;
    private data: IMinuteByMinute[];
    private collectionSize;

    @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

    private _page: number;
    get page(): number {
        return this._page;
    }
    set page(value: number) {
        console.log("set")
        this._page = value;
        this.getTableView(this.data);
    }

    private _pageSize: number;
    get pageSize(): number {
        return this._pageSize;
    }
    set pageSize(value: number) {
        console.log("set page size")
        this._pageSize = value;
        this.page = 1; // back to the first page
    }

    constructor() {
        this.page = 1;
        this.pageSize = 4;
    }

    ngOnInit() {
        this.data = [...json]; // json will be a immutable array
        this.collectionSize = this.data.length;
        this.getTableView(this.data);
        console.log("--->json", this.data.length);
    }

    // private pageData(data: any) {
    //     return this.getTableView(this.data);
    // }

    private getTableView(data: IMinuteByMinute[]): void {

        if (!data ) return;

        const tempArr = this.data.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);

        const rows = [];

        for (const operation of tempArr) {
            const row = {
                data: operation as any,
                items: this.getItems(operation)
            };

            rows.push(row);
        }

        this.tableData = {
            headers: ['shift', 'shiftStart', 'shiftEnd', 'machineId', 'machineName', 'completedAt', 'message'],
            dataSet: rows
        };

        console.log("---> rows", rows.length);
    }

    private getItems(item: IMinuteByMinute): ITableItem[] {

        const tds: ITableItem[] = [];

        const tdShiftName: ITableItem = {
            text: item.shift ? item.shift : null
        };
        const tdShiftStart: ITableItem = {
            text: item.shiftStart ? item.shiftStart : null
        };
        const tdShiftEnd: ITableItem = {
            text: item.shiftEnd ? item.shiftEnd : null
        };
        const tdMachineId: ITableItem = {
            text: item.machineId ? item.machineId : null
        };
        const tdMachineName: ITableItem = {
            text: item.machineName ? item.machineName : null
        };
        const tdCompletedAt: ITableItem = {
            text: item.completedAt ? item.completedAt : null
        };
        const tdMessage: ITableItem = {
            text: item.message ? item.message : null
        };

        // const tdFirstTrain: ITableItem = {
        //   badge: {
        //     text: item.firstTrain ? this.getTime(item.firstTrain) : null,
        //     color: (item.firstTrainRule === 1 || item.firstTrainRule === 0) ? this.getBadgeColor(item.firstTrainRule) : null,
        //     tooltip: `
        //     <p><b>1ER TREN</b></p>
        //     <p>LOCO 137</p>
        //     <p>${this.langService.message('estimated_time')}: <span><b>xxxx</b></span></p>
        //     <p>${this.langService.message('check_in_time')}:
        //       <span class='${this.getSpanClass(item.firstTrainRule)}'>
        //         <b>${this.getTime(item.firstTrainDatetime)}</b>
        //       </span>
        //     </p>
        //    `
        //   }
        // };


        tds.push(tdShiftName);
        tds.push(tdShiftStart);
        tds.push(tdShiftEnd);
        tds.push(tdMachineId);
        tds.push(tdMachineName);
        tds.push(tdCompletedAt);
        tds.push(tdMessage);

        return tds;
    }

    private getTime(value: string): string {
        let date = '';

        if (value) {
            date = moment(value).format('hh:mm:ss');
        }

        return date;
    }

    private getSpanClass(value: number): string {
        let spanClass = '';

        if (value === 1) {
            spanClass = 'mt-tooltip-success-text';
        } else if (value === 0) {
            spanClass = 'mt-tooltip-warning-text';
        }

        return spanClass;
    }

    private getBadgeColor(value: number): string {

        let color = '';

        if (value === 1) {
            return color = '#22D983';
        } else if (value === 0) {
            color = '#F05050';
        }

        return color;
    }

    onSort({ column, direction }: SortEvent) {

        let arr: IMinuteByMinute[] = this.data;
        
        // resetting other headers
        this.headers.forEach(header => {
            if (header.sortable !== column) {
                header.direction = '';
            }
        });

        // sorting data
        if (direction === '') {
            this.data = [...json];
        } else {
            this.data = this.data.sort((a, b) => {
                const res = compare(a[column], b[column]);
                return direction === 'asc' ? res : -res;
            });
        }

        this.page = 1; // back to the first page
    }
}