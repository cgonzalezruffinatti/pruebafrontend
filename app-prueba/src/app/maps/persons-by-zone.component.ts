import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';


const data = [
    {
      "zoneAntennaLocationId": 1,
      "antennaTableId": 1,
      "latitude": 577.125,
      "longitude": 612.1746255889769,
      "relatedLatitude": 577.125,
      "relatedLongitude": 612.1746255889769,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 1,
      "zoneName": "Exterior mina"
    },
    {
      "zoneAntennaLocationId": 2,
      "antennaTableId": 9,
      "latitude": 707.625,
      "longitude": 624.4803302231588,
      "relatedLatitude": 707.625,
      "relatedLongitude": 624.4803302231588,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 1,
      "zoneName": "Exterior mina"
    },
    {
      "zoneAntennaLocationId": 3,
      "antennaTableId": 11,
      "latitude": 816.625,
      "longitude": 694.9132612592468,
      "relatedLatitude": 816.625,
      "relatedLongitude": 694.9132612592468,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 1,
      "zoneName": "Exterior mina"
    },
    {
      "zoneAntennaLocationId": 4,
      "antennaTableId": 13,
      "latitude": 577.125,
      "longitude": 612.1746255889769,
      "relatedLatitude": 577.125,
      "relatedLongitude": 612.1746255889769,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 1,
      "zoneName": "Exterior mina"
    },
    {
      "zoneAntennaLocationId": 5,
      "antennaTableId": 2,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 3,
      "zoneName": "R 680"
    },
    {
      "zoneAntennaLocationId": 6,
      "antennaTableId": 3,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 3,
      "zoneName": "R 680"
    },
    {
      "zoneAntennaLocationId": 7,
      "antennaTableId": 5,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 3,
      "zoneName": "R 680"
    },
    {
      "zoneAntennaLocationId": 8,
      "antennaTableId": 7,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 3,
      "zoneName": "R 680"
    },
    {
      "zoneAntennaLocationId": 9,
      "antennaTableId": 10,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 4,
      "zoneName": "N 730"
    },
    {
      "zoneAntennaLocationId": 10,
      "antennaTableId": 6,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 5,
      "zoneName": "N 680 n"
    },
    {
      "zoneAntennaLocationId": 11,
      "antennaTableId": 4,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 6,
      "zoneName": "N 680 s"
    },
    {
      "zoneAntennaLocationId": 12,
      "antennaTableId": 8,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 7,
      "zoneName": "A 630"
    },
    {
      "zoneAntennaLocationId": 13,
      "antennaTableId": 15,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 7,
      "zoneName": "A 630"
    },
    {
      "zoneAntennaLocationId": 14,
      "antennaTableId": 16,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 8,
      "zoneName": "N 630"
    },
    {
      "zoneAntennaLocationId": 15,
      "antennaTableId": 18,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 9,
      "zoneName": "N 600"
    },
    {
      "zoneAntennaLocationId": 16,
      "antennaTableId": 12,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 10,
      "zoneName": "N 580"
    },
    {
      "zoneAntennaLocationId": 17,
      "antennaTableId": 14,
      "createdAt": "2020-03-24T16:33:10.7734626-03:00",
      "isActive": true,
      "zoneId": 11,
      "zoneName": "N 540"
    }
  ];

@Component({
    templateUrl: 'persons-by-zone.component.html'
})

export class PersonsByZoneComponent implements OnInit {

    groupByZoneId = [];
    personsByZoneId;
    from: number;
    to: number;

    constructor() { }

    ngOnInit() { 
        this.groupByZoneId = _.groupBy(data, 'zoneId'); // objetos agrupados por zona
        this.getPersonsByZoneId(); // resumen de personas por zona
    }

    getPersonsByZoneId() {
        this.personsByZoneId = [];
        // tslint:disable-next-line: forin
        for (const zone in this.groupByZoneId) {
            const obj = { zoneId: zone, persons: this.groupByZoneId[zone].length }
            this.personsByZoneId.push(obj);
        }
    }

    changeZone() {

        if (this.from && this.to) {

            let zoneFrom = this.groupByZoneId[this.from]; // se obtiene grupo de zona origen
            let item = zoneFrom.pop(); // se extrae el ultimo elemento

            let zoneTo = this.groupByZoneId[this.to]; // se obtiene grupo de zona de destino

            if(item) {
                item.zoneId = this.to; // se cambia zoneId de origen a zone id de destino
                zoneTo.push(item);
            }

            this.getPersonsByZoneId();
        }
    }
}