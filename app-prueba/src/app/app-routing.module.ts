import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VistaUnoComponent } from './vista-uno/vista-uno.component';
import { VistaDosComponent } from './vista-dos/vista-dos.component';
import { TrainsComponent } from './trains/trains.component';
import { MinuteByMinuteComponent } from './trains/minutebyminute.component';
import { NgbTableComponent } from './trains/ngbTable.component';
import { PersonsByZoneComponent } from './maps/persons-by-zone.component';

const routes: Routes = [
  { path: 'vista-uno', component: VistaUnoComponent },
  { path: 'vista-dos', component: VistaDosComponent },
  { path: 'trenes', component: TrainsComponent },
  { path: 'minutebyminute', component: MinuteByMinuteComponent },
  { path: 'ngbootstraptable', component: NgbTableComponent },
  { path: 'personsbyzone', component: PersonsByZoneComponent },
  { path: '', redirectTo: 'personsbyzone', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
