import { Component, OnInit } from '@angular/core';
import { DataService } from '../core/data.service';
import { DataResponse } from '../models/dataResponse';
import { TextDetail } from '../models/textDetail';
import { LetterDetail } from '../models/letterDetail';
import * as _ from 'lodash';

@Component({
  selector: 'app-vista-dos',
  templateUrl: './vista-dos.component.html',
  styleUrls: ['./vista-dos.component.css']
})
export class VistaDosComponent implements OnInit {

  loading = false;
  abecedario = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,ñ,o,p,q,r,s,t,u,v,w,x,y,z'.split(',');
  letterList: LetterDetail[][];

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  getData() {
    this.loading = true;
    this.dataService.getDataVistaDos()
    .subscribe((data: DataResponse) => {
      if (data) {
         const list: LetterDetail[][] = [];

         data.data.forEach((value: TextDetail) => {
            list.push(this.processParagraph(value.paragraph));
         });
         this.loading = false;
         this.letterList = list;
      }
    });
  }

  processParagraph(paragraph: string): LetterDetail[] {
    const list: LetterDetail[] = [];

    this.abecedario.forEach((letter) => {
      const letterObj = new LetterDetail();
      const reLetter = new RegExp(letter, 'gi');
      letterObj.paragraph = paragraph;
      letterObj.letter = letter;
      letterObj.quantity = paragraph.match(reLetter) ? paragraph.match(reLetter).length : 0; // cantidad de apariciones por letra
      letterObj.numbers = paragraph.match(/\d+/g); // obtener números dentro del arreglo
      list.push(letterObj)
    });

    return list;
  }

  sum(numbers): number {
    
    let sum = 0;
    if (numbers) {
      numbers.forEach(element => {
        sum += _.toNumber(element);
      });
    }
    return sum;
  }

}
